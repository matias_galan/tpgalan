/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unju.tpGalan.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Alumno
 */
@Entity
@Table(name = "prestamo")
public class Prestamo {
    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    //@GeneratedValue
    @Column(name = "idprestamo")
    private int idprestamo;
    @ManyToOne
    @JoinColumn(name = "nro_id")
    private Estudiante nro_id;
    @ManyToOne
    @JoinColumn(name = "isbn")
    private Libro isbn;
    @Column(name = "fecha_prestamo")
    private String fecha_prestamo;
    //disculpe profe no dio tiempo para estudia la libreria de java.time

    public Prestamo() {
    }

    public Prestamo(int idprestamo, Estudiante nro_id, Libro isbn, String fecha_prestamo) {
        this.idprestamo = idprestamo;
        this.nro_id = nro_id;
        this.isbn = isbn;
        this.fecha_prestamo = fecha_prestamo;
    }

    public int getIdprestamo() {
        return idprestamo;
    }

    public void setIdprestamo(int idprestamo) {
        this.idprestamo = idprestamo;
    }

    public Estudiante getNro_id() {
        return nro_id;
    }

    public void setNro_id(Estudiante nro_id) {
        this.nro_id = nro_id;
    }

    public Libro getIsbn() {
        return isbn;
    }

    public void setIsbn(Libro isbn) {
        this.isbn = isbn;
    }

    public String getFecha_prestamo() {
        return fecha_prestamo;
    }

    public void setFecha_prestamo(String fecha_prestamo) {
        this.fecha_prestamo = fecha_prestamo;
    }
    
            
}
