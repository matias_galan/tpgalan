/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unju.tpGalan.controller;

import com.unju.tpGalan.interfaceService.IlibroService;
import com.unju.tpGalan.entity.Libro;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Alumno
 */
@org.springframework.stereotype.Controller
@RequestMapping
public class ControllerL {
    @Autowired
    private IlibroService service;

    @GetMapping("/listarl")
    public String listar(Model model) {
        List<Libro> empleadosl = service.listarl();
        model.addAttribute("empleadosl", empleadosl);
        return "indexl";
    }
    @GetMapping("/listarl/{isbn}")
	public String listarId(@PathVariable int isbn,Model model) {
                model.addAttribute("empleadol", service.listarId(isbn));
            
		return "forml";
	}
        
    @GetMapping("/newl")
    public String agregarp(Model model) {
        model.addAttribute("empleadol", new Libro());
        return "forml";
    }

    @PostMapping("/savel")
    public String save(@Valid Libro l,Model model) {
        service.savel(l);
        return "redirect:/listarl";
    }
    	@GetMapping("/eliminarl/{isbn}")
	public String delete(@PathVariable int isbn,Model model) {
		service.deletel(isbn);
		return "redirect:/listarl";
	}
        
        @GetMapping("/editarl/{isbn}")
        public String editarp(@PathVariable int isbn, Model model){
           Optional<Libro>empleadol=service.listarId(isbn);
           model.addAttribute("empleadol", empleadol);
           return "forml";
        }
}
