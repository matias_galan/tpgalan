/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unju.tpGalan.controller;

import com.unju.tpGalan.entity.Estudiante;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.unju.tpGalan.interfaceService.IestudianteService;

/**
 *
 * @author alumno
 */
@org.springframework.stereotype.Controller
@RequestMapping
public class ControllerE {
    
    @Autowired
    private IestudianteService service;

    @GetMapping("/listare")
    public String listar(Model model) {
        List<Estudiante> empleadose = service.listare();
        model.addAttribute("empleadose", empleadose);
        return "indexe";
    }
    @GetMapping("/listare/{nro_id}")
	public String listarId(@PathVariable int nro_id,Model model) {
		model.addAttribute("empleadoe", service.listarId(nro_id));
            
		return "forme";
	}
        
    @GetMapping("/newe")
    public String agregare(Model model) {
        
        model.addAttribute("empleadoe", new Estudiante());
        return "forme";
    }

    @PostMapping("/savee")
    public String save(@Valid Estudiante e,Model model) {
        service.savee(e);
        return "redirect:/listare";
    }
    	@GetMapping("/eliminare/{nro_id}")
	public String delete(@PathVariable int nro_id,Model model) {
		service.deletee(nro_id);
		return "redirect:/listare";
	}
        
        @GetMapping("/editarp/{nro_id}")
        public String editarp(@PathVariable int nro_id, Model model){
            Optional<Estudiante>empleadoe=service.listarId(nro_id);
            model.addAttribute("empleadoe", empleadoe);
            return "forme";
        }
    
}
