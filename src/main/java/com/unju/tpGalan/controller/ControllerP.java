/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unju.tpGalan.controller;

import com.unju.tpGalan.entity.Estudiante;
import com.unju.tpGalan.entity.Libro;
import com.unju.tpGalan.entity.Prestamo;
import com.unju.tpGalan.interfaceService.IestudianteService;
import com.unju.tpGalan.interfaceService.IlibroService;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.unju.tpGalan.interfaceService.IprestamoService;

/**
 *
 * @author Alumno
 */
@org.springframework.stereotype.Controller
@RequestMapping
public class ControllerP {
    @Autowired
    private IprestamoService service;
    
    @Autowired
    private IestudianteService service1;
    @Autowired
    private IlibroService service2;

    @GetMapping("/listarp")
    public String listar(Model model) {
        List<Prestamo> empleadosp = service.listarp();
        model.addAttribute("empleadosp", empleadosp);
        return "indexp";
    }
    @GetMapping("/listarp/{idprestamo}")
	public String listarId(@PathVariable int idprestamo,Model model) {
		
                List<Estudiante> estudiante = service1.listare();
                model.addAttribute("estudiante", estudiante);
        
                List<Libro> libro = service2.listarl();
                model.addAttribute("libro", libro);
                
		
                model.addAttribute("empleadop", service.listarId(idprestamo));
            
		return "formp";
	}
        
    @GetMapping("/newp")
    public String agregarp(Model model) {
        
        List<Estudiante> estudiante = service1.listare();
        model.addAttribute("estudiante", estudiante);
        
        List<Libro> libro = service2.listarl();
        model.addAttribute("libro", libro);
        
        model.addAttribute("empleadop", new Prestamo());
        return "formp";
    }
    @PostMapping("/savep")
    public String save(@Valid Prestamo p,Model model) {
        service.savep(p);
        return "redirect:/listarp";
    }
    	@GetMapping("/eliminarp/{idprestamo}")
	public String delete(@PathVariable int idprestamo,Model model) {
		service.deletep(idprestamo);
		return "redirect:/listarp";
	}
        
        @GetMapping("/editarp/{idprestamo}")
        public String editarc(@PathVariable int idprestamo, Model model){
            
            List<Estudiante> estudiante = service1.listare();
            model.addAttribute("estudiante", estudiante);
            System.out.println(estudiante.toString());
            
            List<Libro> libro = service2.listarl();
            model.addAttribute("libro", libro);
            System.out.println(libro.toString());
            
            Optional<Prestamo>empleadop=service.listarId(idprestamo);
            model.addAttribute("empleadop", empleadop);
            return "formp";
        }
}
