/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unju.tpGalan.interfaceService;

import com.unju.tpGalan.entity.Prestamo;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author Alumno
 */
public interface IprestamoService {
    public List<Prestamo>listarp();
    public Optional<Prestamo>listarId(int idprestamo);
    public int savep(Prestamo p);
    public void deletep(int idprestamo);
}
