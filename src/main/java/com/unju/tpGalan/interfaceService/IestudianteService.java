
package com.unju.tpGalan.interfaceService;

import com.unju.tpGalan.entity.Estudiante;
import java.util.List;
import java.util.Optional;


public interface IestudianteService {
    public List<Estudiante>listare();
    public Optional<Estudiante>listarId(int nro_id);
    public int savee(Estudiante e);
    public void deletee(int nro_id);
    
    
}
