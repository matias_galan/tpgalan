package com.unju.tpGalan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TpGalanApplication {

	public static void main(String[] args) {
		SpringApplication.run(TpGalanApplication.class, args);
	}

}
