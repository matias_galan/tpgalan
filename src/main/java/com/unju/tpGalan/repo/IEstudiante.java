
package com.unju.tpGalan.repo;

import com.unju.tpGalan.entity.Estudiante;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IEstudiante extends CrudRepository <Estudiante, Integer> {
    
}
