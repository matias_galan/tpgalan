/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unju.tpGalan.service;

import com.unju.tpGalan.entity.Prestamo;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.unju.tpGalan.repo.IPrestamo;
import com.unju.tpGalan.interfaceService.IprestamoService;

/**
 *
 * @author Alumno
 */
@Service
public class PrestamoService implements IprestamoService{
    @Autowired
    private IPrestamo data;

    @Override
    public List<Prestamo> listarp() {
        return (List<Prestamo>) data.findAll();
    }

    @Override
    public Optional<Prestamo> listarId(int idprestamo) {
       return data.findById(idprestamo);
    }

    @Override
    public int savep(Prestamo p) {
        int res = 0;
        Prestamo empleado = data.save(p);
        if (!empleado.equals(null)) {
            res = 1;
        }
        return res;
    }

    @Override
    public void deletep(int idprestamo) {
        data.deleteById(idprestamo);
    }
}
