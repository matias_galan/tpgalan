package com.unju.tpGalan.service;

import com.unju.tpGalan.entity.Estudiante;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.unju.tpGalan.repo.IEstudiante;
import com.unju.tpGalan.interfaceService.IestudianteService;

@Service
public class EstudianteService implements IestudianteService {

    @Autowired
    private IEstudiante data;

    @Override
    public List<Estudiante> listare() {
        return (List<Estudiante>) data.findAll();
    }

    @Override
    public Optional<Estudiante> listarId(int nro_id) {
       return data.findById(nro_id);
    }

    @Override
    public int savee(Estudiante e) {
        int res = 0;
        Estudiante empleado = data.save(e);
        if (!empleado.equals(null)) {
            res = 1;
        }
        return res;
    }

    @Override
    public void deletee(int nro_id) {
        data.deleteById(nro_id);
    }

}
