/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unju.tpGalan.service;

import com.unju.tpGalan.entity.Libro;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.unju.tpGalan.repo.ILibro;
import com.unju.tpGalan.interfaceService.IlibroService;

/**
 *
 * @author Alumno
 */
@Service
public class LibroService implements IlibroService{
    @Autowired
    private ILibro data;

    @Override
    public List<Libro> listarl() {
        return (List<Libro>) data.findAll();
    }

    @Override
    public Optional<Libro> listarId(int ibsn) {
       return data.findById(ibsn);
    }

    @Override
    public int savel(Libro l) {
        int res = 0;
        Libro empleado = data.save(l);
        if (!empleado.equals(null)) {
            res = 1;
        }
        return res;
    }

    @Override
    public void deletel(int ibsn) {
        data.deleteById(ibsn);
    }
}
